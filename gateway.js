const { ApolloServer } = require("apollo-server");
const { ApolloGateway,RemoteGraphQLDataSource } = require("@apollo/gateway");

/**
 * sets headers for each request sent to services
 */
class AuthenticatedDataSource extends RemoteGraphQLDataSource {
  willSendRequest({ request, context }) {
    // Pass the user's id from the context to underlying services
    request.http.headers.set('reality', context.reality);
  }
  async didReceiveResponse({ response, request, context }) {
    // Parse the Server-Id header and add it to the array on context
    const serverId = response.http.headers.get('Server-Id');
    if (serverId) {
      context.serverIds.push(serverId);
    }
    // Return the response, even when unchanged.
    return response;
  }
}

const gateway = new ApolloGateway({
  // This entire `serviceList` is optional when running in managed federation
  // mode, using Apollo Graph Manager as the source of truth.  In production,
  // using a single source of truth to compose a schema is recommended and
  // prevents composition failures at runtime using schema validation using
  // real usage-based metrics.
  serviceList: [
    { name: "accounts", url: "http://localhost:4001/graphql" },
    { name: "reviews", url: "http://localhost:4002/graphql" },
    { name: "products", url: "http://localhost:4003/graphql" },
    { name: "inventory", url: "http://localhost:4004/graphql" },
    // { name: "flash", url: "http://localhost:9092/graphql"}
  ],

  // Experimental: Enabling this enables the query plan view in Playground.
  __exposeQueryPlanExperimental: false,
  buildService({ name, url }) {
    return new AuthenticatedDataSource({ url });
  },
});

(async () => {
  const server = new ApolloServer({
    gateway,

    // Apollo Graph Manager (previously known as Apollo Engine)
    // When enabled and an `ENGINE_API_KEY` is set in the environment,
    // provides metrics, schema management and trace reporting.
    engine: false,

    // Subscriptions are unsupported but planned for a future Gateway version.
    subscriptions: false,

    context: ({ req }) => {
      const reality = req.headers.reality
      return {reality} ;
    },
    context() {
      return { serverIds: [] };
    },

    plugins: [
      {
        requestDidStart() {
          return {
            willSendResponse({ context, response }) {
              if(response.http.headers){
              // Append our final result to the outgoing response headers
              response.http.headers.set(
                'Server-Id',
                context.serverIds.join(',')
              );
              }
            }
          };
        }
      }
    ]

  });

  server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
  });
})();
